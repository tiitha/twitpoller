# twitpoller
A quick hack to get some interesting feeds from Twitter to my Slack channel.
## how it works
Define the interesting Twitter screen-names in the properties.yaml with correct Twitter API keys and run the script. 
You can add screen-names to the properties file: they're being loaded with every iteration (iteration frequency can be set in the properties file under 'interval'). 


