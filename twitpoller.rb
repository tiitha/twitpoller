#!/usr/bin/env ruby
require 'rubygems'
require 'bundler/setup'
require 'net/http'
require 'openssl'
require 'twitter'
require 'json'
require 'socket'

class Twitpoller
	def initialize(client = nil)
		raise("No Twitter connection established. Exiting.. ") if client.nil?
		@client  = client
	end

	def getTweets(screen_names = [], last_id = 0, destinations = {})
		return if screen_names.length == 0

		twitter_query = "from:" + screen_names.join(' OR from:')

		LOGGER.debug( sprintf("sending query '%s' with last_id '%d' to Twitter", twitter_query, last_id) )

		result = @client.search( twitter_query, {
			:since_id => last_id,
			:count => 10	# TODO:: currently not respecting the count. 
			})

		LOGGER.debug( sprintf("got %d tweets. Going forward w. iteration.. ", result.count) )

		max_id = last_id
		result.each do |tweet|

			LOGGER.debug(sprintf("found item %d", tweet.id))

			unless last_id == 0 # do not tweet everything if last_id is not set.
				_print_tweet(tweet)
				# additional destinations
				_send_tweet_to_slack(tweet, destinations['slack'])		unless destinations['slack'].nil?
				_send_tweet_to_splunk(tweet, destinations['splunk'])	unless destinations['splunk'].nil?
			end
			max_id = tweet.id if tweet.id > max_id

			LOGGER.debug(sprintf("done w. item %d", tweet.id))
		end
		return result.count,max_id
	end

	def _print_tweet(tweet = nil)
		return if tweet.nil?
		LOGGER.debug("printing to stdout..")
		printf("- - - - -\n%4$s: %3$s\n%1$s @ %2$s\n", tweet.created_at, tweet.uri, tweet.full_text, tweet.user.screen_name)
	end

	def _send_tweet_to_splunk(tweet = nil, splunk = {})
		return if splunk['apikey'].nil? or splunk['collector'].nil?
		return if tweet.nil?
		LOGGER.debug(sprintf("sending to splunk, collector %s", splunk['collector']))

		begin
			url = URI.parse(splunk['collector'])

			http = Net::HTTP.new(url.host, url.port)
	 		http.use_ssl = true
	 		http.verify_mode = OpenSSL::SSL::VERIFY_NONE # Not cool! Change this if using in real life

			request = Net::HTTP::Post.new(splunk['collector'])

			msg = {
	 			'event' => { "tweet" => tweet.full_text },
				'host' => Socket.gethostname.downcase,
	 			'time' => Time.now.to_i
	 		}

			request.body = msg.to_json
	 		request.add_field("Content-type", "application/json")
	 		request.add_field("Authorization", sprintf("Splunk %s",splunk['apikey']))
	 
	 		response = http.request(request)
			LOGGER.debug(response)
		rescue Exception => e
			LOGGER.error(e.message)
		end
	end

	def _send_tweet_to_slack(tweet = nil, slack = nil)
		return if slack['hook'].nil?
		return if tweet.nil?

		LOGGER.debug("sending to slack..")
		begin
			message = sprintf('{ "attachments": [ { 
				"color": "#36a64f", 
				"pretext": ":hatched_chick: *%4$s* @ %1$s: %2$s", 
				"text": "%3$s",
				"mrkdwn_in": [ "text", "pretext" ] 
				} ] }', tweet.created_at, tweet.uri, tweet.full_text, tweet.user.screen_name )

			uri = URI.parse(slack['hook'])

			http = Net::HTTP.new(uri.host, uri.port)
			http.use_ssl = true
			http.verify_mode = OpenSSL::SSL::VERIFY_NONE # Not cool! Change this if using in real life

			request = Net::HTTP::Post.new(slack['hook'])
			request.body = message
			request.add_field("Content-type", "application/json")

			response = http.request(request)
			LOGGER.debug(response)
		rescue Exception => e
			LOGGER.error(e.message)
		end
	end
end
