#!/usr/bin/env ruby
require 'logger'
require './twitpoller'
require 'YAML'

# TODO:: Write down the last tweet-ID to the properties file
properties_file = "properties.yaml"
options = YAML.load_file( properties_file )

LOGGER = Logger.new(STDOUT)
LOGGER.level = options["poller"]["debug"] ? Logger::DEBUG : Logger::INFO
LOGGER.info("Start tailing twitter")

client = Twitter::REST::Client.new( options['twitter'] )

interval = options['poller']['interval'] || 30

destinations = options['destinations'] || nil

poller = Twitpoller.new(client)

LOGGER.info("Starting iterations.. ")

loop do
	# reload feeds from file in every iteration
	LOGGER.debug( "reloading properties file.. " )
	o = YAML.load_file( properties_file )

	feeds   = o['poller']['feeds'] 		|| []
	last_id = o['poller']['tweet_id']	|| 0

	LOGGER.debug( sprintf("found %d feeds. Getting tweets.. ", feeds.length) )
	count,last_id = poller.getTweets( feeds, last_id, destinations)

	LOGGER.info( sprintf("Loaded %d Tweets (last tweet_id: %d).", count, last_id) )
	o['poller']['tweet_id'] = last_id

	LOGGER.debug("saving the properties file.. ")
	File.open(properties_file, 'w') {|f| f.write o.to_yaml }

	sleep(interval)
end
