#!/usr/bin/env ruby
require 'benchmark'
WORDS = (1..100).map{ rand(100000).to_s }

s = "kukimuki"
d = 10

N = 1000
# how to write variables to log event
Benchmark.bmbm do |x|

      x.report("sprintf"){
        N.times{ st = sprintf("this %s has the value of %d, you know", s, d) }
      }

      x.report("build string"){
        N.times{ st = "this "+s+" has the value of "+d.to_s+", you know" }
      }

end

# how to concat an array to a string
Benchmark.bmbm do |x|
      x.report("Array#join"){
        N.times{ s = "from:" + WORDS.join(' OR from:') }
      }
      x.report("Array#join 2"){
        N.times{ s = "from:" << WORDS.join(' OR from:') }
      }
      x.report("Array#join 3"){
        N.times{ s = sprintf("from:%s", WORDS.join(' OR from:')) }
      }
end
